/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acme.customers.server.controller;

import com.acme.customers.model.Customer;
import com.acme.customers.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;


@RestController
public class CustomerController {

    public static final Logger logger = LoggerFactory.getLogger(CustomerController.class);
    @Autowired
	  CustomerService customerService;

    //------------------------- [GET]- get All customers ------------------------------------------


    //------------------------- [GET] - get customer by ID  ----------------------------------------


    //------------------------ [POST] -- add new customer - fn : createCustomer()-------------------



    //------------------------- [PUT] - update customer - fn : updateCustomer() ---------------------



    //------------------------- [DELETE] - update customer - fn : updateCustomer() ---------------------


    // -------------------  TBD -----------------------------



}
