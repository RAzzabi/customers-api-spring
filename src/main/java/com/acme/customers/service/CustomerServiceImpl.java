/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acme.customers.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import com.acme.customers.model.Customer;
import java.util.Iterator;
import org.springframework.stereotype.Service;


@Service("customerService")
public class CustomerServiceImpl implements CustomerService{
    private static final AtomicLong counter = new AtomicLong();
    private static List<Customer> customers;

    static{
            customers= populateDummyUsers();
    }

    public List<Customer> getAllCustomers() {
		return customers;
	}

    private static List<Customer> populateDummyUsers(){
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(new Customer(counter.incrementAndGet(),"Roy", "Fielding"));
        customers.add(new Customer(counter.incrementAndGet(),"James", "Gosling"));
        customers.add(new Customer(counter.incrementAndGet(),"Rod", "Johnson"));
        customers.add(new Customer(counter.incrementAndGet(),"Admin", "Miage"));
        return customers;
    }

        

}
