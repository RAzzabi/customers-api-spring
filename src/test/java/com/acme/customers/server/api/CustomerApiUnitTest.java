package com.acme.customers.server.api;

import com.acme.customers.model.Customer;
import java.net.URI;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import javassist.NotFoundException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// @formatter:off
public class CustomerApiUnitTest {

        /*@Autowired
	private MockMvc mvc;
*/      @Autowired
	private TestRestTemplate restTemplate;
        public static final String REST_SERVICE_URI = "http://localhost:8081";


        @Test
	public void helloWorldTest() throws Exception {
            System.out.println("Testing helloWorldTest API-----------");
            String body = this.restTemplate.getForObject("/test/hello", String.class);
            assertEquals("Hello", body);
	}

        /* GET */
        @Test @Ignore
        public void getCustomers(){
            System.out.println("Testing getCustomers API-----------");
            try {
                List<LinkedHashMap<String, Object>> customersMap = this.restTemplate.getForObject("/customers", List.class);

                if(customersMap!=null){
                    for(LinkedHashMap<String, Object> map : customersMap){
                        System.out.println("Customer : id="+map.get("id")+", Firstname="+map.get("firstName")+", Lastname="+map.get("lastName"));;
                    }
                }else{
                    System.out.println("No customer exist----------");
                }

            } catch (HttpClientErrorException e) {
              assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
            } catch(RestClientException e) {
              fail("this isn't the expected exception: "+e.getMessage());
            }
        }


        @Test @Ignore
        public void getCustomerById(){
            System.out.println("Testing getUser API----------");
            try {
                Customer customer = this.restTemplate.getForObject("/customers/1", Customer.class);
                assertEquals("Admin", customer.getFirstName());
                fail("End point not defined");
            } catch (HttpClientErrorException e) {
              assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
            } catch(RestClientException e) {
              fail("this isn't the expected exception: "+e.getMessage());
            }
        }

        /* POST */
        @Test @Ignore
        public void createCustomer() {
            System.out.println("Testing create Customer API----------");
            try {

                Customer new_customer = new Customer(0,"Sarah","BOB");
                // Data attached to the request.
                HttpEntity<Customer> requestBody = new HttpEntity<>(new_customer);
                // Send request with POST method.

                ResponseEntity<Customer> result  = this.restTemplate.postForEntity("/customers/", requestBody, Customer.class);

                System.out.println("Status code:" + result.getStatusCode());

                if (result.getStatusCode() == HttpStatus.CREATED) {
                    System.out.println("user created");
                }else{
                    fail("End point not defined");
                }
            } catch (HttpClientErrorException e) {
              assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
            } catch(RestClientException e) {
              fail("this isn't the expected exception: "+e.getMessage());
            }

        }

        /* PUT */
        @Test @Ignore
        public void updateCustomer() {

            System.out.println("Testing update customer API----------");
            try {
                long custNo = 3;
                Customer updateInfo = new Customer(custNo, "Tom", "Cleck");
                HttpHeaders headers = new HttpHeaders();
                headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
                // Data attached to the request.
                HttpEntity<Customer> requestBody = new HttpEntity<>(updateInfo, headers);
                this.restTemplate.put("/customers/3", requestBody, new Object[] {});

                String resourceUrl = "/customers/" + custNo;
                Customer e = this.restTemplate.getForObject(resourceUrl, Customer.class);
                if (e != null) {
                     System.out.println("Customer: " + e.getFirstName()+ " - " + e.getLastName());
                }
                fail("End point not defined");
            } catch (HttpClientErrorException e) {
                assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
            } catch(RestClientException e) {
                fail("this isn't the expected exception: "+e.getMessage());
            }


        }

        /* DELETE */
        @Test @Ignore
        public void deleteCustomer() {
            System.out.println("Testing delete customer API----------");
            try {
                System.out.println("Testing delete Customer API----------");
                this.restTemplate.delete("/customers/2");
                fail("End point not defined");
            } catch (HttpClientErrorException e) {
                assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
            } catch(RestClientException e) {
                fail("this isn't the expected exception: "+e.getMessage());
            }
        }


        /* DELETE */
        @Test @Ignore
        public void deleteAllCustomer() {
            System.out.println("Testing all delete Customer API----------");
            try {
                this.restTemplate.delete("/customers/");
                fail("End point not defined");
            } catch (HttpClientErrorException e) {
                assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
            } catch(RestClientException e) {
                fail("this isn't the expected exception: "+e.getMessage());
            }
        }


}
